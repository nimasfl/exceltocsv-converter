const fs = require("fs");
const readXlsxFile = require("read-excel-file/node");

if (!fs.existsSync("./files")) fs.mkdirSync("./files");

const allFiles = fs.readdirSync("./files");
let excelFiles = [];

allFiles.forEach(file => {
  const temp = file.split(".")
  const fileExtension = temp[temp.length -1];
  if (fileExtension === "xlsx") excelFiles.push(file)
});

excelFiles.forEach(file => {
  let data = "";
  readXlsxFile(`./files/${file}`)
    .then(rows => {
      rows.forEach(row => {
        data = data + row.toString() + "\n";
      });
    })
    .then(_ => {
      const createdFileName = file.substr(0,file.length-5);
      fs.writeFileSync(`./files/${createdFileName}`, data)
    });
});


